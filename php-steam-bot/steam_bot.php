<?php

/**
 * Function to make trade offer
 * Note that it called from other two functions
 *
 * @param string $steam_id steamid of palyer that recives offer
 * @param string|int $app_id id of the game
 * @param string|int $asset_id id of item you want to give
 * @param string $token player trade URL (optional)
 * @param string $message message for other player (optional)
 * @return string|int Trade offer id
*/
function make_trade_offer( $steam_id, $app_id, $asset_id, $token='', $message='' ){

  // loging libraries
  define('php-steamlogin', true);
  require('php-steamlogin/main.php');
  require('php-steamlogin/log_and_pass.php');
  require ('php-steam-tradeoffers/steam.class.php');

  // setting up items array
  $itemsFromMe = array(
    "appid" => $app_id,
    "contextid" => 2,
    "amount" => 1,
    "assetid" => $asset_id
  );


  // trade offer options
  $options = array (
    'partnerSteamId' => $steam_id,
    'itemsFromMe' => $itemsFromMe,
    'itemsFromThem' => array(),
    'message' => $message
  );

  // if token is passed adding it to $options array
  if($token != ''){

    // setting accessToken
    $options['accessToken'] = $token;

  }

  // setting up steam login class
  $SteamLogin = new SteamLogin(array(
      'username' => $login, // variable comes from "php-steamlogin/log_and_pass.php"
      'password' => $pass, // variable comes from "php-steamlogin/log_and_pass.php"
      'datapath' => dirname(__FILE__) //path to saving cache files
  ));

  // if logged in successed
  if ($SteamLogin->success)
  {
    // logging in with steam
    $logindata = $SteamLogin->login();

    // makint bot object
    $steam = new SteamTrade();

    // setting up bot object
    $steam->setup($logindata['sessionId'], $logindata['cookies']);

    // making trade offer
    $result = $steam->makeOffer($options);

    // returning result
    return $result;

  }
  else {

    // returning eror
    return $SteamLogin->error;

  }

}
