PHP STEAM TRDAE BOT

This is steam bot for simple trade offers that can be modified in various different ways!

Based on:

"SzymonLisowiec/php-steamlogin" => https://github.com/halipso/php-steam-tradeoffers

"halipso/php-steam-tradeoffers" => https://github.com/halipso/php-steam-tradeoffers

Usage example showed in "index.php".

Login and password for a bot is in "php-steamlogin/log_and_pass.php".


In addition independent update "php-update/update.php" class that gets items from your or someone's inventory and sorts them out. Based on steam web API.
Example showed in "php-update/update_index.php".

By using any of this code you automatically agree to => https://steamcommunity.com/dev/apiterms
