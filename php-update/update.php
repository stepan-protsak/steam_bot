<?php

/**
 * Class to get steam inventory and items of player
 */
class Update
{
  /**
  * @var strig|int $steam_id: id of a steam user
  */
  private $steam_id = '';

  /**
   * Constructor
   *
   * @param string|int $steam_id id of a steam user
  */
  function __construct($steam_id)
  {
    $this->steam_id = $steam_id;
  }

  /**
   * Function to get Dota 2 items
   *
   * @return array An array of items from inventory
  */
  public function get_dota_items(){

    $url = file_get_contents("http://steamcommunity.com/profiles/".$this->steam_id."/inventory/json/570/2/");

    $items = json_decode($url, true);

    if ($items['success'] == 1) {
      return $this->filter_items($items);
    }
    else {
      return '';
    }

  }

  /**
   * Function to get CS: GO items
   *
   * @return array An array of items from inventory
  */
  public function get_cs_items(){

    $url = file_get_contents("http://steamcommunity.com/profiles/".$this->steam_id."/inventory/json/730/2/");

    $items = json_decode($url, true);

    if ($items['success'] == 1) {
      return $this->filter_items($items);
    }
    else {
      return '';
    }

  }

  /**
   * Function to get other player inventory items
   *
   * @param string|int $steam_id steam id of palyer that recives offer
   * @param string|int $app_id id of app
   * @return array An array of item from inventory
  */
  public function get_partner_items($app_id, $steam_id){

    $url = file_get_contents("http://steamcommunity.com/profiles/".$steam_id."/inventory/json/".$app_id."/2/");

    $items = json_decode($url, true);

    if ($items['success'] == 1) {
      return $this->filter_items($items);
    }
    else {
      return '';
    }

  }

  /**
   * Function to get item price
   *
   * @param string $name name of item
   * @param string|int $app_id id of app
   * @return string|flaot Starting price of item
  */
  public function get_item_price($name, $app_id){

    $strs = file_get_contents("http://steamcommunity.com/market/priceoverview/?appid=".$app_id."&market_hash_name=".urlencode($name));
    $json = json_decode($strs, true);

    if (isset($json['success'])){
      return $json['lowest_price'];
    }
    else {
      return '';
    }
  }

  /**
   * Function to filter player inventory items, gets only tradable items
   *
   * @param array $items it is an array of items that you want to sort
   *
   * @return array An array of filtered items
  */
  private function filter_items($items){

    $result = array();

    // looping through inventory and getting needed items
    foreach ($items['rgInventory'] as $key => $value) {

      if ($items['rgDescriptions'][$value['classid'].'_'.$value['instanceid']]['tradable'] == 1) {

        if ($items['rgDescriptions'][$value['classid'].'_'.$value['instanceid']]['icon_url_large']){
          $img = 'http://steamcommunity-a.akamaihd.net/economy/image/'.$items['rgDescriptions'][$value['classid'].'_'.$value['instanceid']]['icon_url_large'];
        } else {
          $img = 'http://steamcommunity-a.akamaihd.net/economy/image/'.$items['rgDescriptions'][$value['classid'].'_'.$value['instanceid']]['icon_url'];
        }

        array_push($result, array('id' => $key,
                                  'img' => $img,
                                  'name' => $items['rgDescriptions'][$value['classid'].'_'.$value['instanceid']]['market_hash_name'],
                                  'amount' => $value['amount'],
                                  ));

      }

    }

    return $result;
    
  }

  /**
   * Function to filter player inventory items, gets only tradable items
   *
   * @param sring|int $trade_id id of trade yo want to know info about
   * @param string $api_key steam API key
   * @return object Trade info object
  */
  public function get_trade_offer($trade_id, $api_key='CB9D9BC5593C490E39F8AC309C9CD9F3') {

    // getting trade info
    $url = file_get_contents("https://api.steampowered.com/IEconService/GetTradeOffer/v1/?key=".$api_key."&tradeofferid=".$trade_id."&language=en");

    // decoding
    $result = json_decode($url, true);

    // returning result
    return $result;

  }

}
